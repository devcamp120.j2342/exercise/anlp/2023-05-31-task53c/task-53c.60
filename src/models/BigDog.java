package models;

public class BigDog extends Dog {

    public BigDog(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void greets() {
        System.out.println("Woow!");
    }

    @Override
    public void greets(Dog another) {
        System.out.println("Woooooooow!");
    }
    
    public void greets(BigDog another) {
        System.out.println("Woooooooow!");
    }
}
