package models;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }
    
    @Override
    public String toString() {
        return "Cat[" + super.getName() + ']';
    }

    @Override
    public void greets() {
        System.out.println("Meow!");
    }
}
