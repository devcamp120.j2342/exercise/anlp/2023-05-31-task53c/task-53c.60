import models.BigDog;
import models.Cat;
import models.Dog;

public class App {
    public static void main(String[] args) throws Exception {
        Cat cat1 = new Cat("Mi");
        System.out.println(cat1);
        cat1.greets();


        Dog dog1 = new Dog("Do");
        Dog dog2 = new Dog("Ki");
        System.out.println(dog1);
        dog1.greets();
        dog1.greets(dog2);

        BigDog bigDog1 = new BigDog("La");
        BigDog bigDog2 = new BigDog("Meo");
        System.out.println(bigDog1);
        bigDog1.greets();
        bigDog1.greets(dog1);
        bigDog1.greets(bigDog2);

    }
}
